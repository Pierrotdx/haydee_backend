const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb+srv://Pierrot:YZhVPdUYMXbd7qiq@cluster0-wg2ik.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true})
    .then(() => {
        console.log('Successfully connected to MongoDB Atlas !');
    })
    .catch((err) => {
        console.log('Unable to connect to MongoDB Atlas');
        console.error(err);
    });

app.use(bodyParser.json());
module.exports = app;